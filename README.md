# arc-lamp
led stripes arch lamp _ Led-Streifen-Bogenleuchte

<img src="/20190705-203508.png" alt="TitelBild"/>

Dieses Projekt ist ebenfalls auf https://www.thingiverse.com/thing:4207121 zu finden.

Dieses ist ein Remix von https://www.thingiverse.com/thing:1636934
Vielen Dank und Hochachtung an capone für dieses tolle Design.

Diese Bogenlampe ist schon ein relativ altes Projekt von mir, und eigentlich das erste wo ich mich intensiver mit OpenSCAD beschäftiftigt habe. Seit nun ca. 2 jahren ist die Lampe täglich und zuverlässig in Benutzung, auch deshalb habe ich mich zu dieser Veröffentlichung entschlossen.

Gegenüber dem Original habe ich alles etwas abgerundeter geformt, einen gefälligen Lampenfuss entworfen, und eine optionale Dimmer-Schaltung entwickelt.

Als Leuchtmittel wird ein led-strip-SMD2835 für 12V Betriebsspannung verwendet. Dieser led-strip hat eine Breite von 8mm, benötigt wird eine Länge von 0,5m. Als Lichtfarbe kann warmeiss oder kaltweiss, entsprechend den eigenen Bedürfnissen, gewählt werden.

Wenn sie die Dimmerschaltung nicht selber bauen wollen, benötigen sie noch einen Leitungsdimmer, den sie einfach zwischen Steckernetzteil und Lampe zwischenschalten. Wenn sie die Dimmerschaltung selber bauen möchten, sollten sie schon etwas geübt in Sachen Elektronik sein, also löten und Atmel-AVRs programmieren können.

Als Steckernetzteil habe ich ein altes, ausgedientes Netzteil einer externen Festplatte verwendet. Sie können jedes beliebige Steckernetzteil für 12V verwenden, es sollte ca. 1A strom liefern können.

Folgende Einzelteile müssen gedruckt werden:

     1  arch-lamp-foot-body.stl
     1  arch-lamp-foot-lid.stl
     1  arch-lamp-body-end.stl
     7  arch-lamp-body.stl
     7  arch-lamp-connector.stl
     6  arch-lamp-lid.stl (durchsichtig)
     2  arch-lamp-lid.stl (nicht durchsichtig)
 

Die Erwärmung durch die eingeschalteten LEDs ist nicht sehr groß, daher können sie auch mit PLA-Filamant drucken. Am besten wird dunkeles Filament verwendet (zB. schwarz) damit die LEDs nicht durchscheinen, das sieht nicht wirklich gut aus.

Druckparameter:

     40% Fülldichte 
     0,2mm Schichthöhe
     3 vertikale Konturen 
     3 horizontale Konturhüllen 
     80mm/s Druckgeschwindigkeit 
     Support ist nicht erforderlich.


Nachbearbeitung und Zusammenbau:

Zunächst werden alle Einzelteile mit 100er Schleifpapier sorgfältig geschliffen, dabei geht es nicht nur darum alle Unebenheiten zu beseitigen, sondern auch die Oberfläche für die Lackierung aufzurauhen. Am Besten werden die Einzelteile in der Reihenfolge des Zusammenbaus durchnummeriert, und einzeln so weit bearbeitet das sie gut zusammen passen.

Nachdem die Lampe einmal komplett zusammengesteckt wurde, können sie die Einzelteile ihren eigenen Wünschen entsprechend lackieren.

Als Primer benutze ich Surface-Primer 70.628 von Vallejo.
Für die Lackierung benutze ich Acryl-Farben, und zwar "Vallejo Model Air" die ich in diesem Fall mit dem Pinsel auftrage und nicht mit dem Airbrush. Die verwendeten Farbtöne sind alle in dem Set1 (6 Basisfarben) von Vallejo enthalten.
Für das anschließende Kupfer-Finish benutze ich Wax-Metall-Finish von Rub'nBuff, Farbtönung "Spanish Copper".

Das Kupfer-Finish kommt am besten auf einer konstrastreichen Grundlage zur Geltung.

Ich habe folgendes Mischverhältnis verwendet:

     15 Teile Schwarz 
     7 Teile Gelb 
     4 Teile Rot 


Das Mischverhältnis ändere ich immer ein wenig, um Konstrastunterschiede zu erzeugen. Sehr gut geht auch zu gleichen Teilen Olivgrün mit Schwarz gemischt.

Nachdem alle Farben getrocknet sind, geht es mit mit dem Wax-Metall-Finisher zur Sache.

Es gibt 2 gute Effekte:

1 - Man trägt das Wax-Finish nur auf die Kanten und die hervorragenden Flächen auf. Dieses erzeugt den Effekt als ob die Farbe durch Gebrauch abgenutzt ist, und das blanke Kupfer hervorkommt. Das mach man am besten mit dem Finger, dadurch man hat aber das beste Gefühl beim polieren der Kanten. Keine Sorge die Finger werden ganz einfach wieder mit Seife sauber. Wichtig ist immer nur ganz kleine Mengen mit dem Finger aufzutragen und zu polieren.

2 - das Wax-Finish wird flächig aufgetragen. Wichtig ist dabei das die Grundierung immer wieder durchschimmert. Das mache ich mit einem steifen Pinsel, wo ich die Borsten extra kürze. 

Nachdem alles lackiert ist, geht es an den Zusammenbau. Ich verwende für das Verkleben der Einzelteile "UHU-Hart-Kunststoff". Beginnend bei dem Endstück wird der Lichtbogen verklebt, wobei schrittweise auch immer das Led-Stripe und die durchsichtige Abdeckung eingebaut wird. In den komplett verklebten Lichtbogen, wird man den Led-strip nicht einführen können! Ist der Lichtbogen komplett verklebt und fest, wird er in den Lampenfuß eingeklebt. 

Nach Abschluss der Lötarbeiten ist die Lampe betriebsbereit. Viel Spaß!

Gruß Hardy

---------------------------

This project can also be found at https://www.thingiverse.com/thing:4207121 .

This is a remix from https://www.thingiverse.com/thing:1636934
Many thanks and respect to capone for this great design.

This arc lamp is already a relatively old project of mine, and actually the first one where I got more involved with OpenSCAD. For about 2 years now, the lamp has been in daily and reliable use, which is one of the reasons why I decided to publish it here.

Compared to the original, I have rounded everything a little bit, designed a nice lamp base, and developed an optional dimmer circuit.

As illuminant a led-strip-SMD2835 for 12V operating voltage is used. This led-strip has a width of 8mm, a length of 0,5m is needed. The light color can be warm white or cold white, according to your needs.

If you do not want to build the dimmer circuit yourself, you will need a line dimmer, which you simply connect between the power supply and the lamp. If you want to build the dimmer circuit by yourself, you should have some experience in electronics, i.e. you should be able to solder and program Atmel AVRs.

As power supply I used an old, disused power supply of an external hard disk. You can use any power supply for 12V, it should deliver about 1A current.

The following parts must be printed:

     1  arch-lamp-foot-body.stl 
     1  arch-lamp-foot-lid.stl
     1  arch-lamp-body-end.stl 
     7  arch-lamp-body.stl 
     7  arch-lamp-connector.stl
     6  arch-lamp-lid.stl (transparent) 
     2  arch-lamp-lid.stl (not transparent) 


The heating by the switched on LEDs is not very high, so you can print with PLA filament. It's best to use dark filament (e.g. black) so that the LEDs don't shine through, this doesn't really look good.

Print parameters:

     40% fill density 
     0,2mm layer height 
     3 vertical contours 
     3 horizontal contour envelopes 
     80mm/s Print speed 
     Support is not required. 


Rework and assembly:

First of all, all individual parts are carefully sanded with 100 grit sandpaper. This is not only to remove any unevenness, but also to roughen the surface for painting. It is best to number the parts in the order of assembly, and work each part until it fits well together.

Once the lamp is completely assembled, you can paint the parts according to your own wishes.

As primer I use Surface-Primer 70.628 from Vallejo.
For the painting I use acrylic paints, namely "Vallejo Model Air" which I apply with a brush and not with an airbrush. The used colors are all included in the Set1 (6 basic colors) from Vallejo.
For the following copper finish I use wax metal finish from Rub'nBuff, shade "Spanish Copper".

The copper finish is best shown to advantage on a contrasting base.

I have used the following mixing ratio:

     15 parts black
     7 parts yellow 
     4 parts red 


I always change the mixing ratio a little bit to create differences in contrast. Very good is also to mix olive green with black in equal parts.

After all colors are dried, I use the wax metal finisher.

There are 2 good effects:

1 - You apply the wax finish only on the edges and the excellent surfaces. This creates the effect as if the paint is worn out by use and the bare copper comes out. This is best done with a finger, but this gives the best feeling when polishing the edges. Don't worry, your fingers will simply be cleaned with soap again. It is always important to apply and polish only very small amounts with your finger.

2 - The wax finish is applied over a large area. It is important that the primer always shines through. I do this with a stiff brush, where I trim the bristles extra. 

After everything is painted, I start the assembly. I use "UHU-hard plastic" for the gluing of the individual parts. Beginning with the end piece the arc is glued, whereby step by step always the Led-Stripe and the transparent cover is installed. It will not be possible to insert the Led-strip into the completely glued arc! When the arc is completely glued and fixed, it is glued into the lamp base. 

After the soldering work is finished, the lamp is ready for operation. Have fun!

Greetings Hardy





