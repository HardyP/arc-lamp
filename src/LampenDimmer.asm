;***************************************************************************
;* Dateiname            :"LampenDimmer.asm"
;* Titel                :1MHz Takt
;* Datum                :05.10.2018
;* Version              :1.0
;* Ziel MCU           	:ATtiny45
;***************************************************************************
.include "tn45def.inc"

;***** Globale Definitionen
.equ true=0
.equ false=-1
.equ cPWM_Min=20
.equ cPWM_Max=255
.equ ledBit=0
.equ ledPort=PortB
.equ ledPin=PinB
.equ ledDDR=DDRB
.equ LampeAnAusBit=3
.equ LampeAnAusPort=PortB
.equ LampeAnAusPin=PinB
.equ LampeAnAusDDR=DDRB
.equ PWM_PlusBit=4
.equ PWM_PlusPort=PortB
.equ PWM_PlusPin=PinB
.equ PWM_PlusDDR=DDRB
.equ PWM_MinusBit=1
.equ PWM_MinusPort=PortB
.equ PWM_MinusPin=PinB
.equ PWM_MinusDDR=DDRB

;***** CPU-Register
.def rPWM=r16
.def rPWM_Save=r17


;***** 	Data
.dseg
sMist: .Byte 1

.eseg
ePWM: .db 128

;***** 	Code
.cseg
.org $0000
	rjmp isr_Reset
.org INT0addr ;External Interrupt 0
	reti
.org PCI0addr ;Pin change Interrupt Request 0
	reti
.org OC1Aaddr ;Timer/Counter1 Compare Match 1A
	reti
.org OVF1addr ;Timer/Counter1 Overflow
	reti
.org OVF0addr ;Timer/Counter0 Overflow
	reti
.org ERDYaddr ;EEPROM Ready
	reti
.org ACIaddr ;Analog comparator
	reti
.org ADCCaddr ;ADC Conversion ready
	reti
.org OC1Baddr ;Timer/Counter1 Compare Match B
	reti
.org OC0Aaddr ;Timer/Counter0 Compare Match A
	reti
.org OC0Baddr ;Timer/Counter0 Compare Match B
	reti
.org WDTaddr ;Watchdog Time-out
	rjmp isr_WatchDog
.org USI_STARTaddr ;USI START
	reti
.org USI_OVFaddr ;USI Overflow
	reti

.cseg
isr_Reset:
	ldi xh,HIGH(RAMEND) 
	out SPH,xh
	ldi xl,LOW(RAMEND)
	out SPL,xl
	ldi xl,(1<<SE)+(1<<SM1) ;PowerDown einschalten,SleepMode einschalten
	out MCUCR,xl 
	ldi xl,(1<<PRTIM1)+(1<<PRUSI)+(1<<PRADC) ;Power Reduction
	out PRR,xl
	sbi ledDDR,ledBit
	cbi ledPort,ledBit ;LED ausschalten
	sbi LampeAnAusPort,LampeAnAusBit
	sbi PWM_PlusPort,PWM_PlusBit
	sbi PWM_MinusPort,PWM_MinusBit
	ldi zl,low(ePWM)
   	ldi zh,high(ePWM)
   	rcall EE_Read_Byte

	mov rPWM,r0
	mov rPWM_Save,r0
	ldi xl,(1<<WDCE)+(1<<WDE)
	out WDTCSR,xl
	ldi xl,(1<<WDIE)+(1<<WDP2)+(1<<WDP1)+(1<<WDP0) ;WatchDog alle 2 Sekunden
	out WDTCSR,xl
	sei
	rjmp MainLoop

isr_WatchDog:
	ldi xh,HIGH(RAMEND) 
	out SPH,xh
	ldi xl,LOW(RAMEND)
	out SPL,xl
	wdr
	cli
	ldi xl,(0<<WDRF)
	out MCUSR,xl
	ldi xl,(1<<WDCE)+(1<<WDE)
	out WDTCSR,xl
	ldi xl,(1<<WDIE)+(1<<WDP2)+(1<<WDP1)+(1<<WDP0)
	out WDTCSR,xl
;	rcall ledFlackern
	sei
	rjmp MainLoop

EE_Read_Byte:
;  ldi zl,low(eeDate1)
;  ldi zh,high(eeDate1)
;  rcall EE_Read_Byte (Byte wird in r0 �bergeben)
	sbic EECR,EEPE
	rjmp EE_Read_Byte
	out EEARH,ZH 
	out EEARL,ZL    
	sbi EECR,EERE
	in r0,EEDR
	ret

EE_Write_Byte:
;  ldi zl,low(eeDate1)
;  ldi zh,high(eeDate1)
;  rcall EE_Write_Byte (Byte in r0)
	sbic EECR,EEPE
	rjmp EE_Write_Byte
    cbi EECR,EEPM0
	cbi EECR,EEPM1
	out EEARH,ZH 
	out EEARL,ZL    
	out EEDR,r0
	sbi EECR,EEMPE
	sbi EECR,EEPE
	ret

;1ms Verzoegerungsschleife
Wait1ms:
	push xl
	ldi xl,$C6 ;$C6 bei 1MHz,$19 bei 128kHz
Wait1ms1:
	wdr
	nop
	dec xl
	brne Wait1ms1
	pop xl
	ret

;50ms Verzoegerungsschleife
Wait50ms:
	push xl
	ldi xl,50
Wait50ms1:
	rcall Wait1ms
	dec xl
	brne Wait50ms1
	pop xl
	ret

;40ms Verzoegerungsschleife
Wait40ms:
	push xl
	ldi xl,40
Wait40ms1:
	rcall Wait1ms
	dec xl
	brne Wait40ms1
	pop xl
	ret


MainLoop:
	rcall Wait40ms
	sbis LampeAnAusPin,LampeAnAusBit 
	rjmp MainLoop9
; Eingeschaltet
	ldi xl,(1<<COM0A1)+(1<<WGM01)+(1<<WGM00) ; Timer 0 als Fast-PWM nicht invertiert
	out TCCR0A,xl
	ldi xl,(1<<CS01) ; clk/8
	out TCCR0B,xl
	sbis PWM_PlusPin,PWM_PlusBit
	rjmp MainLoop2
	cpi rPWM,cPWM_Max
	breq MainLoop2
	inc rPWM
MainLoop2: 
	sbis PWM_MinusPin,PWM_MinusBit
	rjmp MainLoop3
	cpi rPWM,cPWM_Min
	breq MainLoop3
	dec rPWM
MainLoop3: 
	out OCR0A,rPWM
	rjmp MainLoop
MainLoop9:
; Ausgeschaltet 
	ldi xl,0 ; PWM-Timer Stop
	out TCCR0A,xl
	out TCCR0B,xl
	cbi ledPort,ledBit
	cp rPWM,rPWM_Save
	breq MainLoopA
	ldi zl,low(ePWM)
   	ldi zh,high(ePWM)
	mov r0,rPWM
	mov rPWM_Save,rPWM
   	rcall EE_Write_Byte

MainLoopA:
	sleep
	rjmp MainLoop9






